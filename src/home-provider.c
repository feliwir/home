/* home-provider.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "home-provider.h"

G_DEFINE_INTERFACE (HomeProvider, home_provider, G_TYPE_OBJECT)

static void
home_provider_default_init (HomeProviderInterface *iface)
{
  /* add properties and signals to the interface here */
}

gboolean
home_provider_search (HomeProvider *self, GError *error)
{
  HomeProviderInterface *iface;

  g_return_val_if_fail (HOME_IS_PROVIDER (self), FALSE);
  g_return_val_if_fail (error == NULL, FALSE);

  iface = HOME_PROVIDER_GET_IFACE (self);
  g_return_val_if_fail (iface->search != NULL, FALSE);
  iface->search (self, error);
  return TRUE;
}

