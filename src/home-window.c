/* home-window.c
 *
 * Copyright 2023 Stephan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "home-provider.h"
#include "home-window.h"
#include "hue/home-hue-provider.h"

struct _HomeWindow
{
  AdwApplicationWindow parent_instance;

  GaClient        *avahi_client;
  HomeHueProvider *hue_provider;

  /* Template widgets */
  GtkHeaderBar *header_bar;
  GtkLabel     *label;
};

G_DEFINE_FINAL_TYPE (HomeWindow, home_window, ADW_TYPE_APPLICATION_WINDOW)

static void
home_window_constructed (GObject *obj)
{
  g_autoptr (GError) error = NULL;
  HomeWindow *self         = HOME_WINDOW (obj);

  self->avahi_client = ga_client_new (GA_CLIENT_FLAG_NO_FLAGS);

  if (! ga_client_start (self->avahi_client, &error))
    {
      g_warning ("HomeWindow: Failed to start Avahi Client");
      if (error != NULL)
        g_warning ("HomeWindow: Error: %s", error->message);
      return;
    }

  g_debug ("HomeWindow: Got avahi client");

  self->hue_provider = home_hue_provider_new (self->avahi_client);

  if (! home_provider_search (HOME_PROVIDER (self->hue_provider), error))
    {
      g_warning ("HomeWindow: Avahi client failed to browse: %s", error->message);
      return;
    }

  g_debug ("HomeWindow: Got avahi browser");

  G_OBJECT_CLASS (home_window_parent_class)->constructed (obj);
}

static void
home_window_finalize (GObject *obj)
{
  HomeWindow *self = HOME_WINDOW (obj);

  g_clear_object (&self->avahi_client);

  G_OBJECT_CLASS (home_window_parent_class)->finalize (obj);
}

static void
home_window_dispose (GObject *obj)
{
  HomeWindow *self = HOME_WINDOW (obj);

  g_object_run_dispose (G_OBJECT (self->avahi_client));

  G_OBJECT_CLASS (home_window_parent_class)->dispose (obj);
}

static void
home_window_class_init (HomeWindowClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = home_window_constructed;
  object_class->finalize    = home_window_finalize;
  object_class->dispose     = home_window_dispose;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/feliwir/home/"
                                               "home-window.ui");
  gtk_widget_class_bind_template_child (widget_class, HomeWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, HomeWindow, label);
}

static void
home_window_init (HomeWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
