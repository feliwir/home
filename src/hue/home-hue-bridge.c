/* home-hue-bridge.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "home-hue-bridge.h"
#include <gio/gio.h>

struct _HomeHueBridge
{
  GObject parent_instance;
  /* Other members, including private data. */
  GSettings *settings;
  gchar     *address;
  gchar     *id;
};

enum
{
  PROP_ADDR = 1,
  PROP_ID   = 2,
  N_PROPERTIES,
};

static GParamSpec *props[N_PROPERTIES] = {
  NULL,
};

G_DEFINE_TYPE (HomeHueBridge, home_hue_bridge, G_TYPE_OBJECT)

static void
home_hue_bridge_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  HomeHueBridge *provider = HOME_HUE_BRIDGE (object);

  switch (prop_id)
    {
    case PROP_ADDR:
      g_value_set_string (value, provider->address);
      break;

    case PROP_ID:
      g_value_set_string (value, provider->id);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
home_hue_bridge_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  HomeHueBridge *provider = HOME_HUE_BRIDGE (object);

  switch (prop_id)
    {
    case PROP_ADDR:
      /* Construct only */
      provider->address = g_value_dup_string (value);
      break;

    case PROP_ID:
      /* Construct only */
      provider->id = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
home_hue_bridge_class_init (HomeHueBridgeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = home_hue_bridge_get_property;
  object_class->set_property = home_hue_bridge_set_property;

  props[PROP_ADDR] = g_param_spec_string ("address", "Address",
                                          "The address of this bridge.", "",
                                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
                                              | G_PARAM_STATIC_STRINGS);
  props[PROP_ID] = g_param_spec_string ("id", "Bridge-ID", "The ID of this bridge.", "",
                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
                                            | G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties (object_class, N_PROPERTIES, props);
}

static void
home_hue_bridge_init (HomeHueBridge *self)
{
  /* Instance variable initialisation code. */
  self->settings = g_settings_new ("org.feliwir.home");
}

/******************************************************************
 * HomeHueBridge public functions
 ******************************************************************/

HomeHueBridge *
home_hue_bridge_new (gchar *addr, gchar *bridge_id)
{
  return g_object_new (HOME_TYPE_HUE_BRIDGE, "address", addr, "id", bridge_id, NULL);
}
