/* home-hue-provider.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "home-hue-provider.h"
#include "home-hue-bridge.h"
#include "home-provider.h"

#include <avahi-gobject/ga-service-browser.h>
#include <avahi-gobject/ga-service-resolver.h>
#include <avahi-common/malloc.h>

#define HUE_SERVICE_TYPE "_hue._tcp"

struct _HomeHueProvider
{
  GObject parent_instance;
  /* Other members, including private data. */
  GaClient         *avahi_client;
  GaServiceBrowser *avahi_browser;
  GList            *bridges;
};

enum
{
  PROP_CLIENT = 1,
  N_PROPERTIES,
};

static GParamSpec *props[N_PROPERTIES] = {
  NULL,
};

static void home_hue_provider_interface_init (HomeProviderInterface *iface);

G_DEFINE_TYPE_WITH_CODE (HomeHueProvider,
                         home_hue_provider,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (HOME_TYPE_PROVIDER, home_hue_provider_interface_init))

static void
home_hue_provider_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  HomeHueProvider *provider = HOME_HUE_PROVIDER (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      g_value_set_object (value, provider->avahi_client);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
home_hue_provider_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  HomeHueProvider *provider = HOME_HUE_PROVIDER (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      /* Construct only */
      provider->avahi_client = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
home_hue_provider_finalize (GObject *object)
{
  HomeHueProvider *provider = HOME_HUE_PROVIDER (object);
  g_clear_object (&provider->avahi_client);

  G_OBJECT_CLASS (home_hue_provider_parent_class)->finalize (object);
}

static void
home_hue_provider_class_init (HomeHueProviderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = home_hue_provider_get_property;
  object_class->set_property = home_hue_provider_set_property;
  object_class->finalize     = home_hue_provider_finalize;

  props[PROP_CLIENT] = g_param_spec_object ("client", "Client", "The AvahiClient used to find sinks.",
                                            GA_TYPE_CLIENT,
                                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY
                                                | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPERTIES, props);
}

static void
resolver_found_cb (GaServiceResolver  *resolver,
                   AvahiIfIndex        iface,
                   GaProtocol          proto,
                   gchar              *name,
                   gchar              *type,
                   gchar              *domain,
                   gchar              *hostname,
                   AvahiAddress       *addr,
                   gint                port,
                   AvahiStringList    *txt,
                   GaLookupResultFlags flags,
                   HomeHueProvider    *provider)
{
  gchar            address[AVAHI_ADDRESS_STR_MAX];
  AvahiStringList *list;
  char            *key, *value;

  g_debug ("HomeHueProvider: Found bridge %s at %s:%d on interface %i", name,
           hostname, port, iface);

  if (avahi_address_snprint (address, sizeof (address), addr) == NULL)
    g_warning ("HomeHueProvider: Failed to convert AvahiAddress to string");

  g_debug ("HomeHueProvider: Resolved %s to %s", hostname, address);

  list = avahi_string_list_find (txt, "bridgeid");
  if (list != NULL)
    {
      avahi_string_list_get_pair (list, &key, &value, NULL);
      provider->bridges = g_list_append (provider->bridges, home_hue_bridge_new (address, value));
      avahi_free (key);
      avahi_free (value);
    }

  g_object_unref (resolver);
}

static void
resolver_failure_cb (GaServiceResolver *resolver, GError *error, HomeHueProvider *provider)
{
  g_warning ("HomeHueProvider: Failed to resolve Avahi service: %s", error->message);
  g_object_unref (resolver);
}

static void
service_added_cb (GaServiceBrowser   *browser,
                  AvahiIfIndex        iface,
                  GaProtocol          proto,
                  gchar              *name,
                  gchar              *type,
                  gchar              *domain,
                  GaLookupResultFlags flags,
                  HomeHueProvider    *provider)
{
  GaServiceResolver *resolver;
  GError            *error = NULL;

  resolver = ga_service_resolver_new (iface, proto, name, type, domain,
                                      GA_PROTOCOL_INET, GA_LOOKUP_NO_FLAGS);

  g_signal_connect (resolver, "found", (GCallback) resolver_found_cb, provider);

  g_signal_connect (resolver, "failure", (GCallback) resolver_failure_cb, provider);

  if (! ga_service_resolver_attach (resolver, provider->avahi_client, &error))
    {
      g_warning ("HomeHueProvider: Failed to attach Avahi resolver: %s", error->message);
      g_error_free (error);
    }
}

static void
service_removed_cb (GaServiceBrowser   *browser,
                    AvahiIfIndex        iface,
                    GaProtocol          proto,
                    gchar              *name,
                    gchar              *type,
                    gchar              *domain,
                    GaLookupResultFlags flags,
                    HomeHueProvider    *provider)
{
  g_debug ("HomeHueProvider: mDNS service \"%s\" removed from interface %i", name, iface);
}

static void
home_hue_provider_init (HomeHueProvider *self)
{
  /* Instance variable initialisation code. */
  self->avahi_browser = ga_service_browser_new (HUE_SERVICE_TYPE);
  g_signal_connect (self->avahi_browser, "new-service", G_CALLBACK (service_added_cb), self);
  g_signal_connect (self->avahi_browser, "removed-service",
                    G_CALLBACK (service_removed_cb), self);
}

/******************************************************************
 * HomeHueProvider interface implementation
 ******************************************************************/

static gboolean home_hue_provider_search (HomeProvider *self, GError *error);

static void
home_hue_provider_interface_init (HomeProviderInterface *iface)
{
  iface->search = home_hue_provider_search;
}

static gboolean
home_hue_provider_search (HomeProvider *self, GError *error)
{
  HomeHueProvider *provider;
  g_return_val_if_fail (HOME_IS_HUE_PROVIDER (self), FALSE);

  provider = HOME_HUE_PROVIDER (self);

  if (provider->avahi_client == NULL)
    {
      g_warning ("HomeHueProvider: No Avahi client found");
      return FALSE;
    }

  if (! ga_service_browser_attach (provider->avahi_browser, provider->avahi_client, &error))
    {
      g_warning ("HomeHueProvider: Failed to attach Avahi Service Browser: %s",
                 error->message);
      return FALSE;
    }

  return TRUE;
}

/******************************************************************
 * HomeHueProvider public functions
 ******************************************************************/

HomeHueProvider *
home_hue_provider_new (GaClient *client)
{
  return g_object_new (HOME_TYPE_HUE_PROVIDER, "client", client, NULL);
}
